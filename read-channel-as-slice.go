package core

// Read a channel into a slice
func ReadChannelAsSlice[T any](ch <-chan T) []T {
	return ReadChannelAsSliceN[T](ch, -1)
}

// Read a channel into a slice
// Will return no more than maxLen
// maxlen = -1 means no limit
func ReadChannelAsSliceN[T any](ch <-chan T, maxLen int) []T {
	result := make([]T, 0, max(maxLen, 0))
	for it := range ch {
		result = append(result, it)
		if maxLen > 0 && len(result) == maxLen {
			break
		}
	}
	return result
}
