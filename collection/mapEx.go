package collection

import (
	"iter"
	"sort"
)

type MapEx[K comparable, V any] map[K]V

func CreateMapFromSlice[K comparable, V any](slice []V, key func(V) K) (m MapEx[K, V]) {
	m = make(map[K]V, len(slice))
	for _, v := range slice {
		m[key(v)] = v
	}
	return
}

func (m MapEx[K, V]) Clone() (newMap MapEx[K, V]) {
	if m != nil {
		newMap = make(map[K]V, len(m))
		for k, v := range m {
			newMap[k] = v
		}
	}
	return
}

func (m MapEx[K, V]) Merge(others ...map[K]V) (newMap MapEx[K, V]) {
	return m.mergeImpl(others, true)
}
func (m MapEx[K, V]) MergeNoOverwrite(others ...map[K]V) (newMap MapEx[K, V]) {
	return m.mergeImpl(others, false)
}
func (m MapEx[K, V]) mergeImpl(others []map[K]V, overwrite bool) (newMap MapEx[K, V]) {
	if m != nil {
		newMap = m.Clone()
	} else {
		newMap = make(map[K]V)
	}
	for _, o := range others {
		for k, v := range o {
			if overwrite {
				newMap[k] = v
			} else if _, ok := newMap[k]; !ok {
				newMap[k] = v
			}
		}
	}
	return
}

func (m MapEx[K, V]) Sorted(less func(i, j K) bool) (seq iter.Seq2[K, V]) {
	return func(yield func(K, V) bool) {
		sortedKeys := make([]K, 0, len(m))
		for k := range m {
			sortedKeys = append(sortedKeys, k)
		}
		sort.Slice(sortedKeys, func(i, j int) bool { return less(sortedKeys[i], sortedKeys[j]) })
		for _, k := range sortedKeys {
			if !yield(k, m[k]) {
				break
			}
		}
	}
}

func (m MapEx[K, V]) Keys() (seq iter.Seq[K]) {
	return func(yield func(K) bool) {
		for k := range m {
			if !yield(k) {
				break
			}
		}
	}
}

func (m MapEx[K, V]) Values() (seq iter.Seq[V]) {
	return func(yield func(V) bool) {
		for _, v := range m {
			if !yield(v) {
				break
			}
		}
	}
}
