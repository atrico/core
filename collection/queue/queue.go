package queue

type Queue[T any] interface {
	Push(item T)
	Pop() (item T)
	TryPop() (item T, ok bool)
	Len() int
}

func NewQueue[T any](items ...T) Queue[T] {
	return &queueImpl[T]{items}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type queueImpl[T any] struct {
	items []T
}

func (q *queueImpl[T]) Push(item T) {
	q.items = append(q.items, item)
}

func (q *queueImpl[T]) Pop() (item T) {
	item = q.items[0]
	q.items = q.items[1:]
	return
}

func (q *queueImpl[T]) TryPop() (item T, ok bool) {
	if ok = q.Len() > 0; ok {
		item = q.Pop()
	}
	return
}

func (q *queueImpl[T]) Len() int {
	return len(q.items)
}
