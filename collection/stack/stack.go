package stack

type Stack[T any] interface {
	Push(item T)
	Pop() (item T)
	Peek() (item T)
	TryPop() (item T, ok bool)
	TryPeek() (item T, ok bool)
	Len() int
}

func NewStack[T any](items ...T) Stack[T] {
	return &stackImpl[T]{items}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type stackImpl[T any] struct {
	items []T
}

func (q *stackImpl[T]) Push(item T) {
	q.items = append(q.items, item)
}

func (q *stackImpl[T]) Pop() (item T) {
	last := len(q.items) - 1
	item = q.items[last]
	q.items = q.items[:last]
	return
}

func (q *stackImpl[T]) Peek() (item T) {
	last := len(q.items) - 1
	item = q.items[last]
	return
}

func (q *stackImpl[T]) TryPop() (item T, ok bool) {
	if ok = q.Len() > 0; ok {
		item = q.Pop()
	}
	return
}

func (q *stackImpl[T]) TryPeek() (item T, ok bool) {
	if ok = q.Len() > 0; ok {
		item = q.Peek()
	}
	return
}

func (q *stackImpl[T]) Len() int {
	return len(q.items)
}
