package set

// Mutable set
type MutableSet[T comparable] interface {
	Set[T]
	// Modify by adding elements
	Add(elements ...T)
	// Modify by removing elements
	Remove(elements ...T)
	// Modify to Difference between sets
	ToDifference(rhs Set[T])
	// Modify to Union of sets
	ToUnion(rhs Set[T])
	// Modify to Intersection of sets
	ToIntersection(rhs Set[T])
	// Modify to Complement the set
	ToComplement()
}

func MakeMutableSet[T comparable](elements ...T) MutableSet[T] {
	return MakeMutableSetWithUniversal[T](nil, elements...)
}

func MakeMutableSetWithUniversal[T comparable](universal Set[T], elements ...T) MutableSet[T] {
	return makeSetImpl[T](elements, universal)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// All modifications should use addToSet and removeFromSet - If not, you must reset the hash cache manually
// ----------------------------------------------------------------------------------------------------------------------------

func (s *setImpl[T]) Add(elements ...T) {
	for _, element := range elements {
		if s.universal != nil && !s.universal.Contains(element) {
			panic(ElementNotInUniversalSet)
		}
		s.addToSet(element)
	}
}

func (s *setImpl[T]) Remove(elements ...T) {
	s.removeFromSet(elements...)
}

func (s *setImpl[T]) ToDifference(rhs Set[T]) {
	s.checkUniversalSet(rhs)
	s.removeFromSetMap(rhs.getMap())
}

func (s *setImpl[T]) ToUnion(rhs Set[T]) {
	s.checkUniversalSet(rhs)
	s.addToSetMap(rhs.getMap())
}

func (s *setImpl[T]) ToIntersection(rhs Set[T]) {
	s.checkUniversalSet(rhs)
	for el := range s.mp {
		if !rhs.Contains(el) {
			s.removeFromSet(el)
		}
	}
}

func (s *setImpl[T]) ToComplement() {
	if s.universal == nil {
		panic(NoUniversalSetPanic)
	}
	s.universal.Foreach(func(element T) {
		if s.Contains(element) {
			s.removeFromSet(element)
		} else {
			s.addToSet(element)
		}
	})
}
