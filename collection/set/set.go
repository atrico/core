package set

import (
	"fmt"
	"gitlab.com/atrico/core/v2"
	"gitlab.com/atrico/core/v2/patterns"
	"hash/fnv"
	"iter"
	"sort"
	"strings"
)

// Immutable set
type Set[T comparable] interface {
	fmt.Stringer
	patterns.Iteratable[T]
	// Set is empty
	IsEmpty() bool
	// Length of set
	Len() int
	// Elements as a slice
	Elements() []T
	// Equals another set (ignore universal)
	Equals(rhs Set[T]) bool
	// Hash code
	Hash() uint64
	// Operate on each element
	Foreach(action func(element T))
	// Does set contain element
	Contains(element T) bool
	// Is subset
	IsSubsetOf(rhs Set[T]) bool
	IsProperSubsetOf(rhs Set[T]) bool
	// Is superset
	IsSupersetOf(rhs Set[T]) bool
	IsProperSupersetOf(rhs Set[T]) bool
	// Add elements
	Plus(elements ...T) Set[T]
	// Remove elements
	Minus(elements ...T) Set[T]
	// Difference between sets
	Difference(rhs Set[T]) Set[T]
	// Union of sets
	Union(rhs Set[T]) Set[T]
	// Intersection of sets
	Intersection(rhs Set[T]) Set[T]
	// Complement the set
	Complement() Set[T]
	// The universal set
	UniversalSet() Set[T]
	// Create a mutable set from this
	CreateMutableSet(increaseCapacity uint) MutableSet[T]

	// Internal
	getMap() map[T]any
}

func MakeSet[T comparable](elements ...T) Set[T] {
	return MakeSetWithUniversal[T](nil, elements...)
}

func MakeSetWithUniversal[T comparable](universal Set[T], elements ...T) Set[T] {
	return makeSetImpl[T](elements, universal)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

func (s setImpl[T]) String() string {
	text := strings.Builder{}
	text.WriteString("{")
	sep := ""
	s.Foreach(func(element T) {
		text.WriteString(fmt.Sprintf("%s%v", sep, element))
		sep = ","
	})
	text.WriteString("}")
	return text.String()
}

func (e CartesianElement[T, U]) String() string {
	return fmt.Sprintf("(%v,%v)", e.First, e.Second)
}

func (s setImpl[T]) Iterator() iter.Seq[T] {
	return func(yield func(T) bool) {
		for el := range s.mp {
			if !yield(el) {
				break
			}
		}
	}
}

func (s setImpl[T]) IsEmpty() bool {
	return s.Len() == 0
}

func (s setImpl[T]) Len() int {
	return len(s.mp)
}

func (s setImpl[T]) Elements() (elements []T) {
	elements = make([]T, len(s.mp))
	i := 0
	for element := range s.mp {
		elements[i] = element
		i++
	}
	return elements
}

func (s *setImpl[T]) Hash() uint64 {
	return s.hash.GetValue()
}

func (s setImpl[T]) Equals(rhs Set[T]) (equal bool) {
	// Check hash first
	if equal = s.Hash() == rhs.Hash(); equal {
		// Check length
		if equal = s.Len() == rhs.Len(); equal {
			// Check elements
			for element := range s.mp {
				if !rhs.Contains(element) {
					return false
				}
			}
		}
	}
	return equal
}

func (s setImpl[T]) Foreach(action func(element T)) {
	for el := range s.mp {
		action(el)
	}
}

func (s setImpl[T]) Contains(el T) bool {
	_, ok := s.mp[el]
	return ok
}

func (s setImpl[T]) IsSubsetOf(rhs Set[T]) bool {
	s.checkUniversalSet(rhs)
	for el := range s.mp {
		if !rhs.Contains(el) {
			return false
		}
	}
	return true
}

func (s setImpl[T]) IsProperSubsetOf(rhs Set[T]) bool {
	return s.IsSubsetOf(rhs) && rhs.Len() > s.Len()
}

func (s setImpl[T]) IsSupersetOf(rhs Set[T]) bool {
	s.checkUniversalSet(rhs)
	for el := range rhs.Iterator() {
		if !s.Contains(el) {
			return false
		}
	}
	return true
}

func (s setImpl[T]) IsProperSupersetOf(rhs Set[T]) bool {
	return s.IsSupersetOf(rhs) && rhs.Len() < s.Len()
}

func (s setImpl[T]) Plus(elements ...T) Set[T] {
	result := s.cloneSet(len(elements))
	result.Add(elements...)
	return result
}

func (s setImpl[T]) Minus(elements ...T) Set[T] {
	result := s.cloneSet(0)
	result.Remove(elements...)
	return result
}

func (s setImpl[T]) Difference(rhs Set[T]) Set[T] {
	s.checkUniversalSet(rhs)
	result := s.cloneSet(0)
	result.removeFromSetMap(rhs.getMap())
	return result
}

func (s setImpl[T]) Union(rhs Set[T]) Set[T] {
	s.checkUniversalSet(rhs)
	result := s.cloneSet(rhs.Len())
	result.addToSetMap(rhs.getMap())
	return result
}

func (s setImpl[T]) Intersection(rhs Set[T]) Set[T] {
	s.checkUniversalSet(rhs)
	result := makeSetImpl[T]([]T{}, s.universal)
	for el := range s.mp {
		if rhs.Contains(el) {
			result.addToSet(el)
		}
	}
	return result
}

func (s setImpl[T]) Complement() Set[T] {
	if s.universal == nil {
		panic(NoUniversalSetPanic)
	}
	result := makeSetImpl[T]([]T{}, s.universal)
	s.universal.Foreach(func(element T) {
		if !s.Contains(element) {
			result.addToSet(element)
		}
	})
	return result
}

func (s setImpl[T]) UniversalSet() Set[T] {
	return s.universal
}

func (s setImpl[T]) CreateMutableSet(increaseCapacity uint) MutableSet[T] {
	return s.cloneSet(int(increaseCapacity))
}

// ----------------------------------------------------------------------------------------------------------------------------
// Internal
// ----------------------------------------------------------------------------------------------------------------------------
type setImpl[T comparable] struct {
	mp        map[T]any
	universal Set[T]
	hash      core.CachedValue[uint64]
}

func makeSetImpl[T comparable](elements []T, universal Set[T]) *setImpl[T] {
	var set setImpl[T]
	set = setImpl[T]{make(map[T]any, len(elements)), universal, core.NewCachedValue(set.calculateHash)}
	set.addToSet(elements...)
	return &set
}

func (s *setImpl[T]) getMap() map[T]any {
	return s.mp
}

func (s *setImpl[T]) cloneSet(increaseCapacity int) *setImpl[T] {
	var set setImpl[T]
	set = setImpl[T]{make(map[T]any, s.Len()+increaseCapacity), s.universal, core.NewCachedValue(set.calculateHash)}
	for el := range s.mp {
		set.mp[el] = nil
	}
	return &set
}

func (s *setImpl[T]) addToSet(elements ...T) {
	for _, element := range elements {
		s.mp[element] = nil
	}
	s.hash.Reset()
}

func (s *setImpl[T]) addToSetMap(mp map[T]any) {
	for element := range mp {
		s.mp[element] = nil
	}
	s.hash.Reset()
}

func (s *setImpl[T]) removeFromSet(elements ...T) {
	for _, element := range elements {
		delete(s.mp, element)
	}
	s.hash.Reset()
}

func (s *setImpl[T]) removeFromSetMap(mp map[T]any) {
	for element := range mp {
		delete(s.mp, element)
	}
	s.hash.Reset()
}

func (s *setImpl[T]) checkUniversalSet(rhs Set[T]) {
	if s.universal != nil {
		var ok bool
		if ok = rhs.UniversalSet() != nil; ok {
			ok = s.universal.Equals(rhs.UniversalSet())
		}
		if !ok {
			panic(UniversalSetMismatch)
		}
	}
}

func (s *setImpl[T]) calculateHash() uint64 {
	elements := make([]string, 0, s.Len())
	s.Foreach(func(element T) { elements = append(elements, fmt.Sprintf("%v", element)) })
	sort.Slice(elements, func(i, j int) bool { return elements[i] < elements[j] })
	hash := fnv.New64a()
	for _, element := range elements {
		hash.Write([]byte(element))
	}
	return hash.Sum64()
}
