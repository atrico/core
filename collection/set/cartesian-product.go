package set

// Create cartesian product
func CartesianProduct[T, U comparable](lhs Set[T], rhs Set[U]) Set[CartesianElement[T, U]] {
	result := makeSetImpl[CartesianElement[T, U]]([]CartesianElement[T, U]{}, nil)
	for elL := range lhs.Iterator() {
		for elR := range rhs.Iterator() {
			result.addToSet(CartesianElement[T, U]{elL, elR})
		}
	}
	return result
}

type CartesianElement[T, U comparable] struct {
	First  T
	Second U
}
