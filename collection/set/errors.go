package set

const NoUniversalSetPanic = "cannot complement without universal set"
const ElementNotInUniversalSet = "element not in universal set"
const UniversalSetMismatch = "sets have different universal sets"
