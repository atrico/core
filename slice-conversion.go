package core

import (
	"errors"
	"fmt"
	"reflect"

	"gitlab.com/atrico/core/v2/errorsEx"
)

// Up Cast a slice of any type to any
func UpCastSlice[T any](input []T) (output []any) {
	output = make([]any, len(input))
	for i, val := range input {
		output[i] = val
	}
	return output
}

// Down Cast a slice of any type to any
func DownCastSlice[T any](input []any) (output []T, err error) {
	errB := errorsEx.NewErrorBuilder("failed to convert")
	var targetType T
	tType := reflect.TypeOf(targetType).String()
	output = make([]T, len(input))

	var ok bool
	for i, val := range input {
		if output[i], ok = val.(T); !ok {
			errB.AddError(errors.New(fmt.Sprintf("could not convert: [%d] %s(%v) to %s", i, reflect.TypeOf(val), val, tType)))
		}
	}
	return output, errB.Build()
}

// Convert a slice of one type to another
func ConvertSlice[TI, TO any](input []TI, convert func(in TI) TO) (output []TO) {
	output = make([]TO, len(input))
	for i, val := range input {
		output[i] = convert(val)
	}
	return output
}
