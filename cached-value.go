package core

type CachedValue[T any] interface {
	GetValue() T
	Reset()
}

func NewCachedValue[T any](creator func() T) CachedValue[T] {
	cv := cachedValueImpl[T]{creator: creator}
	return &cv
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type cachedValueImpl[T any] struct {
	theValue T
	creator  func() T
	hasValue bool
}

func (v *cachedValueImpl[T]) GetValue() T {
	if !v.hasValue {
		v.theValue = v.creator()
		v.hasValue = true
	}
	return v.theValue
}

func (v *cachedValueImpl[T]) Reset() {
	v.hasValue = false
}
