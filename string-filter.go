package core

import (
	"github.com/rs/zerolog/log"
	"regexp"
)

// StringFilter strings based on regex include and exclude
type StringFilter interface {
	// Is this string included (matches filter criteria)
	IsIncluded(item string) bool
	// Filter this slice
	Filter(original []string) (filtered []string)
}

func NewFilter(include []string, exclude []string) StringFilter {
	filter := stringFilter{}
	// Compile includes
	for _, inc := range include {
		filter.Include = append(filter.Include, regexp.MustCompile(inc))
	}
	// Compile excludes
	for _, exc := range exclude {
		filter.Exclude = append(filter.Exclude, regexp.MustCompile(exc))
	}
	return filter
}

var AcceptAllFilter StringFilter = defaultFilter(true)
var RejectAllFilter StringFilter = defaultFilter(false)

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type stringFilter struct {
	Include []*regexp.Regexp
	Exclude []*regexp.Regexp
}

func (f stringFilter) Filter(original []string) (filtered []string) {
	for _, item := range original {
		if f.IsIncluded(item) {
			filtered = append(filtered, item)
		}
	}
	return
}

func (f stringFilter) IsIncluded(item string) bool {
	keep := len(f.Include) == 0
	action := ""
	for i := 0; !keep && i < len(f.Include); i++ {
		keep = f.Include[i].Match([]byte(item))
	}
	if keep {
		if len(f.Include) > 0 {
			action = "In"
		}
		for i := 0; keep && i < len(f.Exclude); i++ {
			if f.Exclude[i].Match([]byte(item)) {
				keep = false
				action = "Ex"
			}
		}
	}
	if action != "" {
		log.Debug().Msgf("%scluding item %s", action, item)
	}
	return keep
}

type defaultFilter bool

func (d defaultFilter) IsIncluded(item string) bool {
	return bool(d)
}

func (d defaultFilter) Filter(original []string) []string {
	if d {
		return original
	} else {
		return []string{}
	}
}
