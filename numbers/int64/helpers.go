package int64Ex

import uint64Ex "gitlab.com/atrico/core/v2/numbers/uint64"

const MinValue = -MaxValue - 1
const MaxValue = int(uint64Ex.MaxValue >> 1)
