package int8Ex

import uint8Ex "gitlab.com/atrico/core/v2/numbers/uint8"

const MinValue = -MaxValue - 1
const MaxValue = int(uint8Ex.MaxValue >> 1)
