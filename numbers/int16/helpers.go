package int16Ex

import uint16Ex "gitlab.com/atrico/core/v2/numbers/uint16"

const MinValue = -MaxValue - 1
const MaxValue = int(uint16Ex.MaxValue >> 1)
