package intEx

import uintEx "gitlab.com/atrico/core/v2/numbers/uint"

const MinValue = -MaxValue - 1
const MaxValue = int(uintEx.MaxValue >> 1)
