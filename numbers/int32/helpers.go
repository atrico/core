package int32Ex

import uint32Ex "gitlab.com/atrico/core/v2/numbers/uint32"

const MinValue = -MaxValue - 1
const MaxValue = int(uint32Ex.MaxValue >> 1)
