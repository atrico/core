package core

import "iter"

// Read a sequence into a slice
func ReadSequenceAsSlice[T any](seq iter.Seq[T]) []T {
	return ReadSequenceAsSliceN[T](seq, -1)
}

// Read a channel into a slice
// Will return no more than maxLen
// maxlen = -1 means no limit
func ReadSequenceAsSliceN[T any](seq iter.Seq[T], maxLen int) []T {
	result := make([]T, 0, max(maxLen, 0))
	for it := range seq {
		result = append(result, it)
		if maxLen > 0 && len(result) == maxLen {
			break
		}
	}
	return result
}

// Read a sequence length
func SequenceLen[T any](seq iter.Seq[T]) (count int) {
	return SequenceLenN(seq, -1)
}

// Read a sequence length with max length
func SequenceLenN[T any](seq iter.Seq[T], maxLen int) (count int) {
	for _ = range seq {
		count++
		if maxLen > 0 && count == maxLen {
			break
		}
	}
	return
}
