package core

import (
	"runtime"
	"strconv"
	"strings"
)

// Id of go routine
// Use for debugging only
func GoId() int {
	buf := make([]byte, 32)
	n := runtime.Stack(buf, false)
	idField := strings.Fields(strings.TrimPrefix(string(buf[:n]), "goroutine "))[0]
	id, _ := strconv.Atoi(idField)
	return id
}
