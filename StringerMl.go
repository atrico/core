package core

import (
	"fmt"
	"io"
	"iter"
	"os"
)

// Multiline version of Stringer
type StringerMl interface {
	// Representation of object as multiple lines
	StringMl(params ...any) iter.Seq[string]
}

func DisplayMultiline(obj StringerMl, params ...any) {
	FdisplayMultiline(os.Stdout, obj, params...)
}

func FdisplayMultiline(writer io.Writer, obj StringerMl, params ...any) {
	for line := range obj.StringMl(params...) {
		fmt.Fprintln(writer, line)
	}
}
