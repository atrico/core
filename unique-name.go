package core

import (
	"fmt"
)

type UniqueNamesContext map[string]int

func (c *UniqueNamesContext) Unique(name string) (uniqueName string) {
	if c == nil {
		*c = make(UniqueNamesContext, 1)
	}
	count := (*c)[name]
	(*c)[name] = count + 1
	if count > 0 {
		return fmt.Sprintf("%s_%02x", name, count)
	}
	return name
}
