package errorsEx

import (
	"fmt"
	"strings"
)

// Extended error with return code
type Error interface {
	error
	Cause() []error
	ReturnCode() int
}

// Create a new error
func New(msg string, returnCode int, causes ...error) (errEx Error) {
	return errorEx{msg, causes, returnCode}
}

// Plus return code to existing error
func AddReturnCode(err error, returnCode int) (errEx Error) {
	if err != nil {
		errEx = errorEx{err.Error(), make([]error, 0), returnCode}
	}
	return errEx
}

// Wrap error (only if it exists)
func Wrap(err error, format string, args ...any) (wrapped Error) {
	if err != nil {
		retCode := GeneralError
		// If errEx, copy return code
		if errEx, ok := err.(Error); ok {
			retCode = errEx.ReturnCode()
		}
		wrapped = New(fmt.Sprintf(format, args...), retCode, err)
	}
	return wrapped
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type errorEx struct {
	message    string
	causes     []error
	returnCode int
}

func (e errorEx) Error() string {
	message := strings.Builder{}
	message.WriteString(e.message)
	for _, cause := range e.causes {
		message.WriteString(fmt.Sprintf("\n\t%s", cause.Error()))
	}
	return message.String()
}

func (e errorEx) Cause() []error {
	return e.causes
}

func (e errorEx) ReturnCode() int {
	return e.returnCode
}
