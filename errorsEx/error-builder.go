package errorsEx

type ErrorBuilder interface {
	WithReturnCode(code int) ErrorBuilder
	AddError(err error) ErrorBuilder
	// Build error, return nil if no errors added
	Build() Error
}

func NewErrorBuilder(message string) ErrorBuilder {
	return &errorBuilder{errorEx{message, nil, GeneralError}}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type errorBuilder struct {
	errorEx
}

func (e *errorBuilder) WithReturnCode(code int) ErrorBuilder {
	e.errorEx.returnCode = code
	return e
}

func (e *errorBuilder) AddError(err error) ErrorBuilder {
	if err != nil {
		e.errorEx.causes = append(e.errorEx.causes, err)
	}
	return e
}

func (e *errorBuilder) Build() Error {
	if len(e.errorEx.causes) > 0 {
		return e.errorEx
	}
	return nil
}
