package errorsEx

// Reserved unix error return codes

const (
	GeneralError         = 1
	MisuseOfShellBuiltin = 2

	FirstUserError = 3

	CommandInvokedCannotExecute = 126
	CommandNotFound             = 127
	InvalidArgumentToExit       = 128
	TerminatedByControlC        = 130
)

func FatalErrorSignal(signal int) int {
	return 128 + signal
}
