package core

// Convert error return function into direct return with panic
func Must[T any](action func() (T, error)) (result T) {
	var err error
	if result, err = action(); err != nil {
		panic(err)
	}
	return result
}

// Convert error into panic (no return)
func MustNotError(action func() error) {
	if err := action(); err != nil {
		panic(err)
	}
}

func Must1[T, P1 any](action func(P1) (T, error), p1 P1) (result T) {
	return Must(func() (T, error) { return action(p1) })
}
func Must2[T, P1, P2 any](action func(P1, P2) (T, error), p1 P1, p2 P2) (result T) {
	return Must(func() (T, error) { return action(p1, p2) })
}
func Must3[T, P1, P2, P3 any](action func(P1, P2, P3) (T, error), p1 P1, p2 P2, p3 P3) (result T) {
	return Must(func() (T, error) { return action(p1, p2, p3) })
}
func Must4[T, P1, P2, P3, P4 any](action func(P1, P2, P3, P4) (T, error), p1 P1, p2 P2, p3 P3, p4 P4) (result T) {
	return Must(func() (T, error) { return action(p1, p2, p3, p4) })
}
func Must5[T, P1, P2, P3, P4, P5 any](action func(P1, P2, P3, P4, P5) (T, error), p1 P1, p2 P2, p3 P3, p4 P4, p5 P5) (result T) {
	return Must(func() (T, error) { return action(p1, p2, p3, p4, p5) })
}
func Must6[T, P1, P2, P3, P4, P5, P6 any](action func(P1, P2, P3, P4, P5, P6) (T, error), p1 P1, p2 P2, p3 P3, p4 P4, p5 P5, p6 P6) (result T) {
	return Must(func() (T, error) { return action(p1, p2, p3, p4, p5, p6) })
}
func Must7[T, P1, P2, P3, P4, P5, P6, P7 any](action func(P1, P2, P3, P4, P5, P6, P7) (T, error), p1 P1, p2 P2, p3 P3, p4 P4, p5 P5, p6 P6, p7 P7) (result T) {
	return Must(func() (T, error) { return action(p1, p2, p3, p4, p5, p6, p7) })
}
func Must8[T, P1, P2, P3, P4, P5, P6, P7, P8 any](action func(P1, P2, P3, P4, P5, P6, P7, P8) (T, error), p1 P1, p2 P2, p3 P3, p4 P4, p5 P5, p6 P6, p7 P7, p8 P8) (result T) {
	return Must(func() (T, error) { return action(p1, p2, p3, p4, p5, p6, p7, p8) })
}

func MustNotError1[P1 any](action func(P1) error, p1 P1) {
	MustNotError(func() error { return action(p1) })
}
func MustNotError2[P1, P2 any](action func(P1, P2) error, p1 P1, p2 P2) {
	MustNotError(func() error { return action(p1, p2) })
}
func MustNotError3[P1, P2, P3 any](action func(P1, P2, P3) error, p1 P1, p2 P2, p3 P3) {
	MustNotError(func() error { return action(p1, p2, p3) })
}
func MustNotError4[P1, P2, P3, P4 any](action func(P1, P2, P3, P4) error, p1 P1, p2 P2, p3 P3, p4 P4) {
	MustNotError(func() error { return action(p1, p2, p3, p4) })
}
func MustNotError5[P1, P2, P3, P4, P5 any](action func(P1, P2, P3, P4, P5) error, p1 P1, p2 P2, p3 P3, p4 P4, p5 P5) {
	MustNotError(func() error { return action(p1, p2, p3, p4, p5) })
}
func MustNotError6[P1, P2, P3, P4, P5, P6 any](action func(P1, P2, P3, P4, P5, P6) error, p1 P1, p2 P2, p3 P3, p4 P4, p5 P5, p6 P6) {
	MustNotError(func() error { return action(p1, p2, p3, p4, p5, p6) })
}
func MustNotError7[P1, P2, P3, P4, P5, P6, P7 any](action func(P1, P2, P3, P4, P5, P6, P7) error, p1 P1, p2 P2, p3 P3, p4 P4, p5 P5, p6 P6, p7 P7) {
	MustNotError(func() error { return action(p1, p2, p3, p4, p5, p6, p7) })
}
func MustNotError8[P1, P2, P3, P4, P5, P6, P7, P8 any](action func(P1, P2, P3, P4, P5, P6, P7, P8) error, p1 P1, p2 P2, p3 P3, p4 P4, p5 P5, p6 P6, p7 P7, p8 P8) {
	MustNotError(func() error { return action(p1, p2, p3, p4, p5, p6, p7, p8) })
}
