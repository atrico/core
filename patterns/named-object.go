package patterns

// NamedObject has a name
type NamedObject interface {
	Name() string
}

type NamedObjectImpl string

func (n NamedObjectImpl) Name() string {
	return string(n)
}
