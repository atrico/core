package patterns

// Cloneable pattern
type Cloneable[T any] interface {
	// Clone creates a new instance of the object
	Clone() T
}
