package patterns

import (
	"iter"
)

// Iteratable object
type Iteratable[T any] interface {
	Iterator() iter.Seq[T]
}

// Iteratable2 object (2 values)
type Iteratable2[T, U any] interface {
	Iterator() iter.Seq2[T, U]
}

// IteratableIdx object (with index)
type IteratableIdx[T any] interface {
	Iterator() iter.Seq2[int, T]
}
