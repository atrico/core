package patterns

// Comparable pattern
type Comparable[T any] interface {
	// Compare l<r:<0, l=r: 0, l>r:>0
	Compare(rhs T) int
}
