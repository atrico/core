package patterns

// Builder pattern
type Builder[T any] interface {
	Build() T
}
