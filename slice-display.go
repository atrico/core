package core

import (
	"fmt"
	"strings"
)

func DisplaySliceAsSeparatedString[T any](values []T, separator string) string {
	text := strings.Builder{}
	for i, value := range values {
		if i > 0 {
			text.WriteString(separator)
		}
		text.WriteString(fmt.Sprintf("%v", value))
	}
	return text.String()
}
