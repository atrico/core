package unit_tests

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/atrico/core/v2/errorsEx"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Error_New(t *testing.T) {
	// Arrange
	msg := rg.String()
	retCode := rg.Int()
	cause1 := errors.New(rg.String())
	cause2 := errors.New(rg.String())
	cause3 := errors.New(rg.String())
	expMsg := fmt.Sprintf("%s\n\t%s\n\t%s\n\t%s", msg, cause1, cause2, cause3)

	// Act
	errEx := errorsEx.New(msg, retCode, cause1, cause2, cause3)

	// Assert
	assert.That(t, errEx.Error(), is.EqualTo(expMsg), "message")
	assert.That(t, errEx.ReturnCode(), is.EqualTo(retCode), "return code")
	assert.That(t, errEx.Cause(), is.DeepEqualTo([]error{cause1, cause2, cause3}), "cause")
}

func Test_Error_AddReturnCode(t *testing.T) {
	// Arrange
	err := errors.New(rg.String())
	retCode := rg.Int()

	// Act
	errEx := errorsEx.AddReturnCode(err, retCode)

	// Assert
	assert.That(t, errEx.Error(), is.EqualTo(err.Error()), "message")
	assert.That(t, errEx.ReturnCode(), is.EqualTo(retCode), "return code")
	assert.That(t, len(errEx.Cause()), is.EqualTo(0), "cause")
}

func Test_Error_WrapNil(t *testing.T) {
	// Arrange

	// Act
	errEx := errorsEx.Wrap(nil, "error")

	// Assert
	assert.That[any](t, errEx, is.Nil, "no error")
}

func Test_Error_WrapStdError(t *testing.T) {
	// Arrange
	err := errors.New(rg.String())
	expMsg := fmt.Sprintf("%s\n\t%s", "error", err.Error())

	// Act
	errEx := errorsEx.Wrap(err, "error")

	// Assert
	assert.That(t, errEx.Error(), is.EqualTo(expMsg), "message")
	assert.That(t, errEx.ReturnCode(), is.EqualTo(errorsEx.GeneralError), "return code")
	assert.That(t, errEx.Cause(), is.EqualTo([]error{err}), "cause")
}

func Test_Error_WrapErrorEx(t *testing.T) {
	// Arrange
	retCode := rg.Int()
	err := errorsEx.New(rg.String(), retCode)
	expMsg := fmt.Sprintf("%s\n\t%s", "error", err.Error())

	// Act
	errEx := errorsEx.Wrap(err, "error")

	// Assert
	assert.That(t, errEx.Error(), is.EqualTo(expMsg), "message")
	assert.That(t, errEx.ReturnCode(), is.EqualTo(retCode), "return code")
	assert.That(t, errEx.Cause(), is.EqualTo([]error{err}), "cause")
}

func Test_Error_Builder(t *testing.T) {
	// Arrange
	msg := rg.String()
	retCode := rg.Int()
	cause1 := errors.New(rg.String())
	cause2 := errors.New(rg.String())
	cause3 := errors.New(rg.String())
	expMsg := fmt.Sprintf("%s\n\t%s\n\t%s\n\t%s", msg, cause1, cause2, cause3)

	// Act
	errEx := errorsEx.NewErrorBuilder(msg).
		WithReturnCode(retCode).
		AddError(cause1).
		AddError(cause2).
		AddError(cause3).
		Build()

	// Assert
	assert.That(t, errEx.Error(), is.EqualTo(expMsg), "message")
	assert.That(t, errEx.ReturnCode(), is.EqualTo(retCode), "return code")
	assert.That(t, errEx.Cause(), is.DeepEqualTo([]error{cause1, cause2, cause3}), "cause")
}

func Test_Error_BuilderNoCause(t *testing.T) {
	// Arrange
	msg := rg.String()
	retCode := rg.Int()

	// Act
	errEx := errorsEx.NewErrorBuilder(msg).
		WithReturnCode(retCode).
		Build()

	// Assert
	assert.That[any](t, errEx, is.Nil, "no error")
}
