package unit_tests

import (
	"gitlab.com/atrico/core/v2"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_UniqueName_AlreadyUnique(t *testing.T) {
	// Arrange
	ctx := core.UniqueNamesContext{}
	strs := []string{"one", "two", "three"}

	// Act
	results := make([]string, len(strs))
	for i, str := range strs {
		results[i] = ctx.Unique(str)
	}
	// Assert
	for i, result := range results {
		assert.That(t, result, is.EqualTo(strs[i]), "result")
	}
}

func Test_UniqueName_Multiple(t *testing.T) {
	// Arrange
	ctx := core.UniqueNamesContext{}

	// Act
	result0 := ctx.Unique("name")
	result1 := ctx.Unique("name")
	result2 := ctx.Unique("name")

	// Assert
	assert.That(t, result0, is.EqualTo("name"), "first")
	assert.That(t, result1, is.EqualTo("name_01"), "second")
	assert.That(t, result2, is.EqualTo("name_02"), "third")
}
