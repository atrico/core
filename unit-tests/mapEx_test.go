package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/core/v2/collection"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"slices"
	"testing"
)

func Test_MapEx_Regression(t *testing.T) {
	// Check MapEx can be used as a map
	// Init
	mp := collection.MapEx[string, int]{
		"one": 1,
		"two": 2,
	}
	// Set
	mp["three"] = 3
	assert.That(t, len(mp), is.EqualTo(3), "Set and len")
	// Get (exists)
	v, ok := mp["two"]
	assert.That(t, ok, is.True, "Get (exists) - ok")
	assert.That(t, v, is.EqualTo(2), "Get (exists)")
	// Get (missing)
	v, ok = mp["four"]
	assert.That(t, ok, is.False, "Get (missing) - ok")
	// Range
	var k string
	for k, v = range mp {
		switch k {
		case "one":
			assert.That(t, v, is.EqualTo(1), "Range - one")
		case "two":
			assert.That(t, v, is.EqualTo(2), "Range - two")
		case "three":
			assert.That(t, v, is.EqualTo(3), "Range - three")
		default:
			assert.Fail(t, "Range - Wrong key")
		}
	}
	// Delete
	delete(mp, "one")
	assert.That(t, len(mp), is.EqualTo(2), "Delete")
}

func Test_MapEx_CreateFromSlice(t *testing.T) {
	// Arrange
	slc := []string{"one", "to", "three"}

	// Act
	result := collection.CreateMapFromSlice[int, string](slc, func(s string) int { return len(s) })

	// Assert
	assert.That[map[int]string](t, result, is.EqualTo(map[int]string{3: "one", 2: "to", 5: "three"}), "CreateFromSlice")
}

func Test_MapEx_Clone(t *testing.T) {
	// Arrange
	mp := collection.MapEx[string, int]{
		"one":   1,
		"two":   2,
		"three": 3,
	}

	// Act
	mp2 := mp.Clone()
	delete(mp, "two")

	// Assert
	list := make([]string, 0, len(mp2))
	for k, v := range mp2 {
		list = append(list, fmt.Sprintf("%s:%d", k, v))
	}
	assert.That(t, list, is.EquivalentTo([]string{"one:1", "two:2", "three:3"}), "Clone")
}

func Test_MapEx_SortedRange(t *testing.T) {
	// Arrange
	mp := collection.MapEx[string, int]{
		"b": 2,
		"c": 3,
		"a": 1,
	}

	// Act
	list := make([]string, 0, len(mp))
	for k, v := range mp.Sorted(func(i, j string) bool { return i < j }) {
		list = append(list, fmt.Sprintf("%s:%d", k, v))
	}

	// Assert
	assert.That(t, list, is.EqualTo([]string{"a:1", "b:2", "c:3"}), "Sorted")
}

func Test_MapEx_Merge(t *testing.T) {
	// Arrange
	mp1 := collection.MapEx[int, string]{1: "one", 2: "two", 3: "three"}
	mp2 := collection.MapEx[int, string]{1: "ONE", 4: "FOUR"}
	mp3 := collection.MapEx[int, string]{1: "o", 5: "five"}

	// Act
	mp4 := mp1.Merge(mp2, mp3)
	mp4N := mp1.MergeNoOverwrite(mp2, mp3)

	// Assert
	assert.That(t, mp4, is.EqualTo(collection.MapEx[int, string]{1: "o", 2: "two", 3: "three", 4: "FOUR", 5: "five"}), "Merge")
	assert.That(t, mp4N, is.EqualTo(collection.MapEx[int, string]{1: "one", 2: "two", 3: "three", 4: "FOUR", 5: "five"}), "No overwrite")
}

func Test_MapEx_Keys(t *testing.T) {
	// Arrange
	mp := collection.MapEx[int, string]{1: "one", 2: "two", 3: "three"}

	// Act
	result := slices.Collect(mp.Keys())

	// Assert
	assert.That(t, result, is.EquivalentTo([]int{1, 2, 3}), "Keys")
}

func Test_MapEx_Values(t *testing.T) {
	// Arrange
	mp := collection.MapEx[int, string]{1: "one", 2: "two", 3: "three"}

	// Act
	result := slices.Collect(mp.Values())

	// Assert
	assert.That(t, result, is.EquivalentTo([]string{"one", "two", "three"}), "Keys")
}
