package unit_tests

import (
	"gitlab.com/atrico/core/v2/collection/stack"
	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"testing"
)

func Test_Stack_Empty(t *testing.T) {
	// Arrange
	q := stack.NewStack[string]()

	// Act
	l := q.Len()
	_, canPop := q.TryPop()
	ex := PanicCatcher(func() {
		q.Pop()
	})

	// Assert
	assert.That(t, l, is.EqualTo(0), "Length")
	assert.That(t, canPop, is.False, "TryPop")
	assert.That(t, ex, is.NotNil, "Pop exception")
}

func Test_Stack_Push(t *testing.T) {
	// Arrange
	item1 := rg.String()
	item2 := rg.String()
	q := stack.NewStack[string]()
	q.Push(item1)
	q.Push(item2)

	// Act
	l1 := q.Len()
	i1, canPop := q.TryPop()
	l2 := q.Len()
	i2 := q.Pop()
	l3 := q.Len()

	// Assert
	assert.That(t, l1, is.EqualTo(2), "Length1")
	assert.That(t, l2, is.EqualTo(1), "Length2")
	assert.That(t, l3, is.EqualTo(0), "Length3")
	assert.That(t, canPop, is.True, "TryPop")
	assert.That(t, i1, is.EqualTo(item2), "TryPop item")
	assert.That(t, i2, is.EqualTo(item1), "Pop item")
}
