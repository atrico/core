package unit_tests

import (
	"gitlab.com/atrico/core/v2"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

type TestSubObject struct {
	String string
	Int    int
}

type TestObject struct {
	Int    int
	Float  float64
	String string
	Array  []int
	Sub1   TestSubObject
	Sub2   TestSubObject
}

func Test_CloneWithDefaults_AllSet(t *testing.T) {
	// Arrange
	original := createTestObject()
	defaults := TestObject{}
	expected := original

	// Act
	result := core.CloneWithDefaults(original, defaults)

	// Assert
	assert.That(t, result, is.DeepEqualTo(expected), "result")
}

func Test_CloneWithDefaults_NoneSet(t *testing.T) {
	// Arrange
	original := TestObject{}
	defaults := createTestObject()
	expected := defaults

	// Act
	result := core.CloneWithDefaults(original, defaults)

	// Assert
	assert.That(t, result, is.DeepEqualTo(expected), "result")
}

func Test_CloneWithDefaults_Mixed(t *testing.T) {
	// Arrange
	original := createTestObject()
	defaults := createTestObject()
	expected := original
	original.Int = 0
	expected.Int = defaults.Int
	original.Array = nil
	expected.Array = defaults.Array
	// Default whole struct
	original.Sub1 = TestSubObject{}
	expected.Sub1 = defaults.Sub1
	// Default single field in struct
	original.Sub2.String = ""
	expected.Sub2.String = defaults.Sub2.String

	// Act
	result := core.CloneWithDefaults(original, defaults)

	// Assert
	assert.That(t, result, is.DeepEqualTo(expected), "result")
}

func Test_CloneWithDefaults_SkipFields(t *testing.T) {
	// Arrange
	original := createTestObject()
	defaults := createTestObject()
	expected := original
	original.Int = 0
	expected.Int = defaults.Int
	original.Array = nil
	expected.Array = nil
	// Default whole struct
	original.Sub1 = TestSubObject{}
	expected.Sub1 = defaults.Sub1
	// Default single field in struct
	original.Sub2.String = ""
	expected.Sub2.String = defaults.Sub2.String

	// Act
	result := core.CloneWithDefaults(original, defaults, "Array")

	// Assert
	assert.That(t, result, is.DeepEqualTo(expected), "result")
}

func Test_CloneWithDefaults_SkipSubField(t *testing.T) {
	// Arrange
	original := createTestObject()
	defaults := createTestObject()
	expected := original
	original.Int = 0
	expected.Int = defaults.Int
	original.Array = nil
	expected.Array = defaults.Array
	// Default whole struct
	original.Sub1 = TestSubObject{}
	expected.Sub1 = defaults.Sub1
	// Default single field in struct
	original.Sub2.String = ""
	expected.Sub2.String = ""

	// Act
	result := core.CloneWithDefaults(original, defaults, "Sub2.String")

	// Assert
	assert.That(t, result, is.DeepEqualTo(expected), "result")
}

type TestObjectNonExported struct {
	Int     int
	str     string
	String2 string
}

func Test_CloneWithDefaults_NonExportedFields(t *testing.T) {
	// Arrange
	original := TestObjectNonExported{rg.Int(), "", ""}
	defaults := TestObjectNonExported{rg.Int(), rg.String(), rg.String()}
	expected := original
	expected.String2 = defaults.String2

	// Act
	result := core.CloneWithDefaults(original, defaults)

	// Assert
	assert.That(t, result, is.DeepEqualTo(expected), "result")
}

func createTestObject() (testObj TestObject) {
	return TestObject{
		rg.Int(),
		rg.Float64(),
		rg.String(),
		[]int{rg.Int(), rg.Int(), rg.Int()},
		TestSubObject{rg.String(), rg.Int()},
		TestSubObject{rg.String(), rg.Int()},
	}
}
