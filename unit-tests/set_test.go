package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/core/v2"
	"reflect"
	"testing"

	"gitlab.com/atrico/core/v2/collection/set"
	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Set_Empty(t *testing.T) {
	// Arrange

	// Act
	st := set.MakeSet[int]()

	// Assert
	AssertSet(t, st, []int{})
}

func Test_Set_Initial(t *testing.T) {
	// Arrange
	contents := []int{1, 2, 3}
	// Act
	st := set.MakeSet[int](contents...)

	// Assert
	AssertSet(t, st, contents)
}

func Test_Set_Iterator(t *testing.T) {
	// Arrange
	contents := []int{1, 2, 3}
	st := set.MakeSet[int](contents...)

	// Act
	var result []int
	for el := range st.Iterator() {
		result = append(result, el)
	}

	// Assert
	assert.That[[]int](t, result, is.EquivalentTo(contents), "contents")
}

func Test_Set_Elements(t *testing.T) {
	// Arrange
	contents := []int{1, 2, 3}
	st := set.MakeSet[int](contents...)

	// Act
	result := st.Elements()

	// Assert
	assert.That[[]int](t, result, is.EquivalentTo(contents), "contents")
}

func Test_Set_Foreach(t *testing.T) {
	// Arrange
	contents := []int{1, 2, 3}
	st := set.MakeSet[int](contents...)

	// Act
	var result []int
	st.Foreach(func(element int) { result = append(result, element) })

	// Assert
	assert.That[[]int](t, result, is.EquivalentTo(contents), "contents")
}

func Test_Set_SubsetOf(t *testing.T) {
	// Arrange
	set1 := set.MakeSet(1, 2, 3)
	set2 := set1.Plus(4, 5)

	// Act
	resultSub11 := set1.IsSubsetOf(set1)
	resultSub12 := set1.IsSubsetOf(set2)
	resultSub21 := set2.IsSubsetOf(set1)
	resultProperSub11 := set1.IsProperSubsetOf(set1)
	resultProperSub12 := set1.IsProperSubsetOf(set2)
	resultProperSub21 := set2.IsProperSubsetOf(set1)

	// Assert
	assert.That(t, resultSub11, is.True, "subset of self")
	assert.That(t, resultSub12, is.True, "subset of superset")
	assert.That(t, resultSub21, is.False, "subset of subset")
	assert.That(t, resultProperSub11, is.False, "proper subset of self")
	assert.That(t, resultProperSub12, is.True, "proper subset of superset")
	assert.That(t, resultProperSub21, is.False, "proper subset of subset")
}

func Test_Set_SupersetOf(t *testing.T) {
	// Arrange
	set2 := set.MakeSet(1, 2, 3)
	set1 := set2.Plus(4, 5)

	// Act
	resultSuper11 := set1.IsSupersetOf(set1)
	resultSuper12 := set1.IsSupersetOf(set2)
	resultSuper21 := set2.IsSupersetOf(set1)
	resultProperSuper11 := set1.IsProperSupersetOf(set1)
	resultProperSuper12 := set1.IsProperSupersetOf(set2)
	resultProperSuper21 := set2.IsProperSupersetOf(set1)

	// Assert
	assert.That(t, resultSuper11, is.True, "superset of self")
	assert.That(t, resultSuper12, is.True, "superset of superset")
	assert.That(t, resultSuper21, is.False, "superset of superset")
	assert.That(t, resultProperSuper11, is.False, "proper superset of self")
	assert.That(t, resultProperSuper12, is.True, "proper superset of superset")
	assert.That(t, resultProperSuper21, is.False, "proper superset of superset")
}

func Test_Set_Plus(t *testing.T) {
	// Arrange

	// Act
	st := set.MakeSet[int](1).
		Plus(3).
		Plus(2, 3)

	// Assert
	AssertSet(t, st, []int{1, 2, 3})
}
func Test_Set_Add(t *testing.T) {
	// Arrange

	// Act
	st := set.MakeMutableSet[int](1)
	st.Add(3)
	st.Add(2, 3)

	// Assert
	AssertSet[int](t, st, []int{1, 2, 3})
}

func Test_Set_Minus(t *testing.T) {
	// Arrange

	// Act
	st := set.MakeSet[rune]('a', 'b', 'c', 'd', 'e').
		Minus('b').
		Minus('d', 'e')

	// Assert
	AssertSet(t, st, []rune{'a', 'c'})
}
func Test_Set_Remove(t *testing.T) {
	// Arrange

	// Act
	st := set.MakeMutableSet[rune]('a', 'b', 'c', 'd', 'e')
	st.Remove('b')
	st.Remove('d', 'e')

	// Assert
	AssertSet[rune](t, st, []rune{'a', 'c'})
}

func Test_Set_Difference(t *testing.T) {
	// Arrange

	// Act
	st := set.MakeSet[rune]('a', 'b', 'c', 'd', 'e').
		Difference(set.MakeSet[rune]('b', 'd', 'e'))

	// Assert
	AssertSet(t, st, []rune{'a', 'c'})
}
func Test_Set_ToDifference(t *testing.T) {
	// Arrange

	// Act
	st := set.MakeMutableSet[rune]('a', 'b', 'c', 'd', 'e')
	st.ToDifference(set.MakeSet[rune]('b', 'd', 'e'))

	// Assert
	AssertSet[rune](t, st, []rune{'a', 'c'})
}

func Test_Set_Union(t *testing.T) {
	// Arrange
	set1 := set.MakeSet[string]("one", "two")
	set2 := set.MakeSet[string]("one", "three")

	// Act
	result := set1.Union(set2)

	// Assert
	AssertSet(t, result, []string{"one", "two", "three"})
}
func Test_Set_ToUnion(t *testing.T) {
	// Arrange
	st := set.MakeMutableSet[string]("one", "two")

	// Act
	st.ToUnion(set.MakeSet[string]("one", "three"))

	// Assert
	AssertSet[string](t, st, []string{"one", "two", "three"})
}

func Test_Set_Intersection(t *testing.T) {
	// Arrange
	set1 := set.MakeSet[string]("one", "two")
	set2 := set.MakeSet[string]("one", "three")

	// Act
	result := set1.Intersection(set2)

	// Assert
	AssertSet(t, result, []string{"one"})
}
func Test_Set_ToIntersection(t *testing.T) {
	// Arrange
	st := set.MakeMutableSet[string]("one", "two")

	// Act
	st.ToIntersection(set.MakeSet[string]("one", "three"))

	// Assert
	AssertSet[string](t, st, []string{"one"})
}

func Test_Set_ComplementWithoutUniversal(t *testing.T) {
	// Arrange
	st := set.MakeMutableSet[string]("b", "c")

	// Act
	err1 := PanicCatcher(func() {
		st.Complement()
	})
	err2 := PanicCatcher(func() {
		st.ToComplement()
	})

	// Assert
	assert.That(t, err1, is.Type(reflect.TypeOf(set.NoUniversalSetPanic)), "type")
	assert.That(t, err1.(string), is.EqualTo(set.NoUniversalSetPanic), "content")
	assert.That(t, err2, is.Type(reflect.TypeOf(set.NoUniversalSetPanic)), "type (mutable)")
	assert.That(t, err2.(string), is.EqualTo(set.NoUniversalSetPanic), "content (mutable)")
}

// ----------------------------------------------------------------------------------------------------------------------------
// With Universal set
// ----------------------------------------------------------------------------------------------------------------------------

func Test_Set_UniversalSet(t *testing.T) {
	// Arrange
	universal := set.MakeSet[string]("a", "b", "c", "d", "e")
	st := set.MakeSetWithUniversal[string](universal, "b", "c")

	// Act
	result := st.UniversalSet()

	// Assert
	AssertSet2(t, result, universal, "")
}

func Test_Set_UniversalSetNoSet(t *testing.T) {
	// Arrange
	st := set.MakeSet[string]("b", "c")

	// Act
	result := st.UniversalSet()

	// Assert
	assert.That[any](t, result, is.Nil, "no universal set")
}

func Test_Set_Complement(t *testing.T) {
	// Arrange
	universal := set.MakeSet[string]("a", "b", "c", "d", "e")
	st := set.MakeSetWithUniversal[string](universal, "b", "c")

	// Act
	result := st.Complement()

	// Assert
	AssertSet(t, result, []string{"a", "d", "e"})
}
func Test_Set_ToComplement(t *testing.T) {
	// Arrange
	universal := set.MakeSet[string]("a", "b", "c", "d", "e")
	st := set.MakeMutableSetWithUniversal[string](universal, "b", "c")

	// Act
	st.ToComplement()

	// Assert
	AssertSet[string](t, st, []string{"a", "d", "e"})
}

func Test_Set_AddElementNotInUniversalSet(t *testing.T) {
	// Arrange
	universal := set.MakeSet[string]("a", "b", "c", "d", "e")
	st := set.MakeMutableSetWithUniversal[string](universal)

	// Act
	err1 := PanicCatcher(func() {
		st.Plus("f")
	})
	err2 := PanicCatcher(func() {
		st.Add("f")
	})

	// Assert
	assert.That(t, err1, is.Type(reflect.TypeOf(set.ElementNotInUniversalSet)), "type")
	assert.That(t, err1.(string), is.EqualTo(set.ElementNotInUniversalSet), "content")
	assert.That(t, err2, is.Type(reflect.TypeOf(set.ElementNotInUniversalSet)), "type (mutable)")
	assert.That(t, err2.(string), is.EqualTo(set.ElementNotInUniversalSet), "content (mutable)")
}

func Test_Set_OperatorDifferingUniversalSet(t *testing.T) {
	for name, operation := range setTestCases {
		t.Run(name, func(t *testing.T) {
			// Arrange
			setN := set.MakeMutableSet[int]()
			universal1 := set.MakeSet[int](1, 2, 3)
			set1 := set.MakeMutableSetWithUniversal[int](universal1)
			set1a := set.MakeMutableSetWithUniversal[int](universal1)
			universal2 := set.MakeSet[int](1, 3)
			set2 := set.MakeMutableSetWithUniversal[int](universal2)

			// Act
			err12 := PanicCatcher(func() { operation(set1, set2) })
			err11 := PanicCatcher(func() { operation(set1, set1a) })
			err1N := PanicCatcher(func() { operation(set1, setN) })
			errN1 := PanicCatcher(func() { operation(setN, set1) })

			// Assert
			assert.That(t, err12, is.Type(reflect.TypeOf(set.UniversalSetMismatch)), "12: err type")
			assert.That(t, err12.(string), is.EqualTo(set.UniversalSetMismatch), "12: err content")
			assert.That(t, err11, is.Nil, "11: no error")
			assert.That(t, err1N, is.Type(reflect.TypeOf(set.UniversalSetMismatch)), "1N: err type")
			assert.That(t, err1N.(string), is.EqualTo(set.UniversalSetMismatch), "1N: err content")
			assert.That(t, errN1, is.Nil, "N1: no error")
		})
	}
}

var setTestCases = map[string]func(lhs, rhs set.MutableSet[int]){
	"IsSubsetOf":         func(lhs, rhs set.MutableSet[int]) { lhs.IsSubsetOf(rhs) },
	"IsProperSubsetOf":   func(lhs, rhs set.MutableSet[int]) { lhs.IsProperSubsetOf(rhs) },
	"IsSupersetOf":       func(lhs, rhs set.MutableSet[int]) { lhs.IsSupersetOf(rhs) },
	"IsProperSupersetOf": func(lhs, rhs set.MutableSet[int]) { lhs.IsProperSupersetOf(rhs) },
	"Difference":         func(lhs, rhs set.MutableSet[int]) { lhs.Difference(rhs) },
	"ToDifference":       func(lhs, rhs set.MutableSet[int]) { lhs.ToDifference(rhs) },
	"Union":              func(lhs, rhs set.MutableSet[int]) { lhs.Union(rhs) },
	"ToUnion":            func(lhs, rhs set.MutableSet[int]) { lhs.ToUnion(rhs) },
	"Intersection":       func(lhs, rhs set.MutableSet[int]) { lhs.Intersection(rhs) },
	"ToIntersection":     func(lhs, rhs set.MutableSet[int]) { lhs.ToIntersection(rhs) },
}

// ----------------------------------------------------------------------------------------------------------------------------
// Misc
// ----------------------------------------------------------------------------------------------------------------------------
func Test_Set_CartesianProduct(t *testing.T) {
	// Arrange
	lhs := set.MakeSet[int](1, 2, 3)
	rhs := set.MakeSet[string]("a", "b", "c")

	// Act
	result := set.CartesianProduct(lhs, rhs)

	// Assert
	assert.That(t, result.Len(), is.EqualTo(lhs.Len()*rhs.Len()), "length")
	for elL := range lhs.Iterator() {
		for elR := range rhs.Iterator() {
			element := set.CartesianElement[int, string]{First: elL, Second: elR}
			assert.That(t, result.Contains(element), is.True, "contains %v", element)
		}
	}
}

func Test_Set_DeMorgan(t *testing.T) {
	// Arrange
	universal := set.MakeSet(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
	setA := set.MakeSetWithUniversal(universal, 3, 4, 5, 6, 7)
	setB := set.MakeSetWithUniversal(universal, 4, 5, 6, 7, 8)

	// Act
	// Law of union (A u B)' = A' n B'
	lawOfUnionLhs := setA.Union(setB).Complement()
	lawOfUnionRhs := setA.Complement().Intersection(setB.Complement())
	// Law of intersection (A n B)' = A' u B'
	lawOfIntersectionLhs := setA.Intersection(setB).Complement()
	lawOfIntersectionRhs := setA.Complement().Union(setB.Complement())

	// Assert
	AssertSet2[int](t, lawOfUnionLhs, lawOfUnionRhs, "law of union")
	AssertSet2[int](t, lawOfIntersectionLhs, lawOfIntersectionRhs, "law of intersection")
}

// ----------------------------------------------------------------------------------------------------------------------------
// Assertion
// ----------------------------------------------------------------------------------------------------------------------------

func assertSetImpl[T comparable](t *testing.T, actual set.Set[T], expected []T, msg string) {
	format := "%s"
	if msg != "" {
		format = fmt.Sprintf("%s: %s", msg, format)
		fmt.Println(msg)
	}
	fmt.Println(actual)
	fmt.Println(set.MakeSet(expected...))
	if len(expected) == 0 {
		assert.That(t, actual.IsEmpty(), is.True, format, "empty")
	} else {
		assert.That(t, actual.IsEmpty(), is.False, format, "not empty")
	}
	assert.That(t, actual.Len(), is.EqualTo(len(expected)), format, "len")
	assert.That[[]T](t, core.ReadSequenceAsSlice(actual.Iterator()), is.EquivalentTo(expected), format, "elements")
	for _, el := range expected {
		assert.That(t, actual.Contains(el), is.True, format, fmt.Sprintf("%v", el))
	}
}

func AssertSet[T comparable](t *testing.T, actual set.Set[T], expected []T) {
	assertSetImpl[T](t, actual, expected, "")
}
func AssertSet2[T comparable](t *testing.T, actual, expected set.Set[T], msg string) {
	assertSetImpl[T](t, actual, core.ReadSequenceAsSlice(expected.Iterator()), msg)
}
