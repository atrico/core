package unit_tests

import (
	"errors"
	"gitlab.com/atrico/core/v2"
	"testing"

	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Must(t *testing.T) {
	// Arrange
	expected := rg.String()
	expectedPanic := rg.String()
	fn := func() (string, error) { return expected, nil }
	fnP := func() (string, error) { return expected, errors.New(expectedPanic) }

	// Act
	result := core.Must(fn)
	resultP := PanicCatcher(func() { core.Must(fnP) })

	// Assert
	assert.That(t, result, is.EqualTo(expected), "result")
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_Must1(t *testing.T) {
	// Arrange
	expected := rg.String()
	expectedPanic := rg.String()
	fn := func(int) (string, error) { return expected, nil }
	fnP := func(int) (string, error) { return expected, errors.New(expectedPanic) }

	// Act
	result := core.Must1(fn, rg.Int())
	resultP := PanicCatcher(func() { core.Must1(fnP, rg.Int()) })

	// Assert
	assert.That(t, result, is.EqualTo(expected), "result")
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_Must2(t *testing.T) {
	// Arrange
	expected := rg.String()
	expectedPanic := rg.String()
	fn := func(int, int) (string, error) { return expected, nil }
	fnP := func(int, int) (string, error) { return expected, errors.New(expectedPanic) }

	// Act
	result := core.Must2(fn, 1, 2)
	resultP := PanicCatcher(func() { core.Must2(fnP, 1, 2) })

	// Assert
	assert.That(t, result, is.EqualTo(expected), "result")
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_Must3(t *testing.T) {
	// Arrange
	expected := rg.String()
	expectedPanic := rg.String()
	fn := func(int, int, int) (string, error) { return expected, nil }
	fnP := func(int, int, int) (string, error) { return expected, errors.New(expectedPanic) }

	// Act
	result := core.Must3(fn, 1, 2, 3)
	resultP := PanicCatcher(func() { core.Must3(fnP, 1, 2, 3) })

	// Assert
	assert.That(t, result, is.EqualTo(expected), "result")
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_Must4(t *testing.T) {
	// Arrange
	expected := rg.String()
	expectedPanic := rg.String()
	fn := func(int, int, int, int) (string, error) { return expected, nil }
	fnP := func(int, int, int, int) (string, error) { return expected, errors.New(expectedPanic) }

	// Act
	result := core.Must4(fn, 1, 2, 3, 4)
	resultP := PanicCatcher(func() { core.Must4(fnP, 1, 2, 3, 4) })

	// Assert
	assert.That(t, result, is.EqualTo(expected), "result")
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_Must5(t *testing.T) {
	// Arrange
	expected := rg.String()
	expectedPanic := rg.String()
	fn := func(int, int, int, int, int) (string, error) { return expected, nil }
	fnP := func(int, int, int, int, int) (string, error) {
		return expected, errors.New(expectedPanic)
	}

	// Act
	result := core.Must5(fn, 1, 2, 3, 4, 5)
	resultP := PanicCatcher(func() { core.Must5(fnP, 1, 2, 3, 4, 5) })

	// Assert
	assert.That(t, result, is.EqualTo(expected), "result")
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_Must6(t *testing.T) {
	// Arrange
	expected := rg.String()
	expectedPanic := rg.String()
	fn := func(int, int, int, int, int, int) (string, error) { return expected, nil }
	fnP := func(int, int, int, int, int, int) (string, error) {
		return expected, errors.New(expectedPanic)
	}

	// Act
	result := core.Must6(fn, 1, 2, 3, 4, 5, 6)
	resultP := PanicCatcher(func() { core.Must6(fnP, 1, 2, 3, 4, 5, 6) })

	// Assert
	assert.That(t, result, is.EqualTo(expected), "result")
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_Must7(t *testing.T) {
	// Arrange
	expected := rg.String()
	expectedPanic := rg.String()
	fn := func(int, int, int, int, int, int, int) (string, error) { return expected, nil }
	fnP := func(int, int, int, int, int, int, int) (string, error) {
		return expected, errors.New(expectedPanic)
	}

	// Act
	result := core.Must7(fn, 1, 2, 3, 4, 5, 6, 7)
	resultP := PanicCatcher(func() { core.Must7(fnP, 1, 2, 3, 4, 5, 6, 7) })

	// Assert
	assert.That(t, result, is.EqualTo(expected), "result")
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_Must8(t *testing.T) {
	// Arrange
	expected := rg.String()
	expectedPanic := rg.String()
	fn := func(int, int, int, int, int, int, int, int) (string, error) { return expected, nil }
	fnP := func(int, int, int, int, int, int, int, int) (string, error) {
		return expected, errors.New(expectedPanic)
	}

	// Act
	result := core.Must8(fn, 1, 2, 3, 4, 5, 6, 7, 8)
	resultP := PanicCatcher(func() { core.Must8(fnP, 1, 2, 3, 4, 5, 6, 7, 8) })

	// Assert
	assert.That(t, result, is.EqualTo(expected), "result")
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_MustNotError(t *testing.T) {
	// Arrange
	expectedPanic := rg.String()
	fn := func() error { return nil }
	fnP := func() error { return errors.New(expectedPanic) }

	// Act
	core.MustNotError(fn)
	resultP := PanicCatcher(func() { core.MustNotError(fnP) })

	// Assert
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_MustNotError1(t *testing.T) {
	// Arrange
	expectedPanic := rg.String()
	fn := func(int) error { return nil }
	fnP := func(int) error { return errors.New(expectedPanic) }

	// Act
	core.MustNotError1(fn, 1)
	resultP := PanicCatcher(func() { core.MustNotError1(fnP, 1) })

	// Assert
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_MustNotError2(t *testing.T) {
	// Arrange
	expectedPanic := rg.String()
	fn := func(int, int) error { return nil }
	fnP := func(int, int) error { return errors.New(expectedPanic) }

	// Act
	core.MustNotError2(fn, 1, 2)
	resultP := PanicCatcher(func() { core.MustNotError2(fnP, 1, 2) })

	// Assert
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_MustNotError3(t *testing.T) {
	// Arrange
	expectedPanic := rg.String()
	fn := func(int, int, int) error { return nil }
	fnP := func(int, int, int) error { return errors.New(expectedPanic) }

	// Act
	core.MustNotError3(fn, 1, 2, 3)
	resultP := PanicCatcher(func() { core.MustNotError3(fnP, 1, 2, 3) })

	// Assert
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_MustNotError4(t *testing.T) {
	// Arrange
	expectedPanic := rg.String()
	fn := func(int, int, int, int) error { return nil }
	fnP := func(int, int, int, int) error { return errors.New(expectedPanic) }

	// Act
	core.MustNotError4(fn, 1, 2, 3, 4)
	resultP := PanicCatcher(func() { core.MustNotError4(fnP, 1, 2, 3, 4) })

	// Assert
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_MustNotError5(t *testing.T) {
	// Arrange
	expectedPanic := rg.String()
	fn := func(int, int, int, int, int) error { return nil }
	fnP := func(int, int, int, int, int) error { return errors.New(expectedPanic) }

	// Act
	core.MustNotError5(fn, 1, 2, 3, 4, 5)
	resultP := PanicCatcher(func() { core.MustNotError5(fnP, 1, 2, 3, 4, 5) })

	// Assert
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_MustNotError6(t *testing.T) {
	// Arrange
	expectedPanic := rg.String()
	fn := func(int, int, int, int, int, int) error { return nil }
	fnP := func(int, int, int, int, int, int) error { return errors.New(expectedPanic) }

	// Act
	core.MustNotError6(fn, 1, 2, 3, 4, 5, 6)
	resultP := PanicCatcher(func() { core.MustNotError6(fnP, 1, 2, 3, 4, 5, 6) })

	// Assert
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_MustNotError7(t *testing.T) {
	// Arrange
	expectedPanic := rg.String()
	fn := func(int, int, int, int, int, int, int) error { return nil }
	fnP := func(int, int, int, int, int, int, int) error { return errors.New(expectedPanic) }

	// Act
	core.MustNotError7(fn, 1, 2, 3, 4, 5, 6, 7)
	resultP := PanicCatcher(func() { core.MustNotError7(fnP, 1, 2, 3, 4, 5, 6, 7) })

	// Assert
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}

func Test_MustNotError8(t *testing.T) {
	// Arrange
	expectedPanic := rg.String()
	fn := func(int, int, int, int, int, int, int, int) error { return nil }
	fnP := func(int, int, int, int, int, int, int, int) error { return errors.New(expectedPanic) }

	// Act
	core.MustNotError8(fn, 1, 2, 3, 4, 5, 6, 7, 8)
	resultP := PanicCatcher(func() { core.MustNotError8(fnP, 1, 2, 3, 4, 5, 6, 7, 8) })

	// Assert
	assert.That(t, resultP.(error).Error(), is.EqualTo(expectedPanic), "panic")
}
