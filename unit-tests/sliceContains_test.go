package unit_tests

import (
	"gitlab.com/atrico/core/v2"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_SliceCompare_Empty(t *testing.T) {
	// Arrange
	slice := []string{}

	// Act
	result := core.SliceContains(slice, rg.String())

	// Assert
	assert.That(t, result, is.False, "contains")
}

func Test_SliceCompare_Present(t *testing.T) {
	// Arrange
	item := rg.String()
	slice := []string{rg.String(), rg.String(), item, rg.String()}

	// Act
	result := core.SliceContains(slice, item)

	// Assert
	assert.That(t, result, is.True, "contains")
}

func Test_SliceCompare_Missing(t *testing.T) {
	// Arrange
	slice := []string{rg.String(), rg.String(), rg.String()}

	// Act
	result := core.SliceContains(slice, rg.String())

	// Assert
	assert.That(t, result, is.False, "contains")
}
