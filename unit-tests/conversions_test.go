package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/core/v2"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Conversion_UpCastSlice_String(t *testing.T) {
	// Arrange
	var output []any
	var input []string
	rg.Value(&input)

	// Act
	output = core.UpCastSlice(input)

	// Assert
	for i, v := range input {
		assert.That(t, fmt.Sprintf("%v", output[i]), is.EqualTo(v), "Item %d", i)
	}
}

func Test_Conversion_DownCastSlice_IntoInt(t *testing.T) {
	// Arrange
	var output []int
	input := []any{
		rg.Int(),
		rg.Int(),
		rg.Int(),
	}

	// Act
	var err error
	output, err = core.DownCastSlice[int](input)

	// Assert
	assert.That[any](t, err, is.Nil, "No error")
	for i, v := range input {
		assert.That[int](t, output[i], is.EqualTo(v.(int)), "Item %d", i)
	}
}

func Test_Conversion_DownCastSlice_IntToString(t *testing.T) {
	// Arrange
	input := []any{
		rg.Int(),
		rg.Int(),
		rg.Int(),
	}

	// Act
	var err error
	_, err = core.DownCastSlice[string](input)
	t.Log(err)

	// Assert
	assert.That[any](t, err, is.NotNil, "Error")
}

func Test_Conversion_ConvertSlice_IntToString(t *testing.T) {
	// Arrange
	var output []string
	input := []int{
		rg.Int(),
		rg.Int(),
		rg.Int(),
	}
	rg.Value(&input)

	// Act
	output = core.ConvertSlice(input, func(i int) string { return fmt.Sprintf("%v", i) })

	// Assert
	for i, v := range input {
		assert.That(t, output[i], is.EqualTo(fmt.Sprintf("%v", v)), "Item %d", i)
	}
}
