package unit_tests

import (
	"gitlab.com/atrico/core/v2"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_CachedValue_CreatedOnce(t *testing.T) {
	// Arrange
	counter := new(callCounter)
	cache := core.NewCachedValue[string](func() string { return counter.CreateString() })

	// Act
	val1 := cache.GetValue()
	val2 := cache.GetValue()
	val3 := cache.GetValue()

	// Assert
	assert.That(t, counter.GetCount(), is.EqualTo(1), "create count")
	assert.That(t, val2, is.EqualTo(val1), "same value 1")
	assert.That(t, val3, is.EqualTo(val2), "same value 2")
}

func Test_CachedValue_Reset(t *testing.T) {
	// Arrange
	counter := new(callCounter)
	cache := core.NewCachedValue[string](func() string { return counter.CreateString() })

	// Act
	cache.GetValue()
	cache.Reset()
	val2 := cache.GetValue()
	val3 := cache.GetValue()

	// Assert
	assert.That(t, counter.GetCount(), is.EqualTo(2), "create count")
	assert.That(t, val3, is.EqualTo(val2), "same value")
}

type callCounter int

func (c *callCounter) GetCount() int {
	return int(*c)
}

func (c *callCounter) CreateString() string {
	*c++
	return rg.String()
}
