package unit_tests

import (
	"bytes"
	"io"
	"testing"

	"gitlab.com/atrico/core/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_ByteReader_Empty(t *testing.T) {
	// Arrange
	var expected []byte
	reader, readerN := makeTestReaders(expected, 5)

	// Act
	buffer := make([]byte, 10)
	n, err := core.ReadBytes(reader, buffer)
	bufferN, errN := core.ReadNBytes(readerN, 10)

	// Assert
	assert.That(t, n, is.EqualTo(0), "count")
	assert.That(t, err, is.EqualTo(io.EOF), "error")
	assert.That(t, len(bufferN), is.EqualTo(0), "bufferN")
	assert.That(t, errN, is.EqualTo(io.EOF), "errorN")
}

func Test_ByteReader_LessThanRequired(t *testing.T) {
	// Arrange
	expected := []byte{1, 2, 3}
	reader, readerN := makeTestReaders(expected, 5)

	// Act
	buffer := make([]byte, 10)
	n, err := core.ReadBytes(reader, buffer)
	bufferN, errN := core.ReadNBytes(readerN, 10)

	// Assert
	assert.That(t, n, is.EqualTo(3), "count")
	assert.That(t, buffer[0:3], is.DeepEqualTo(expected), "buffer")
	assert.That(t, err, is.EqualTo(io.EOF), "error")
	assert.That(t, bufferN, is.DeepEqualTo(expected), "bufferN")
	assert.That(t, errN, is.EqualTo(io.EOF), "errorN")
}

func Test_ByteReader_MoreThanRequired(t *testing.T) {
	// Arrange
	expected := []byte{1, 2, 3}
	reader, readerN := makeTestReaders(expected, 5)

	// Act
	buffer := make([]byte, 3)
	n, err := core.ReadBytes(reader, buffer)
	bufferN, errN := core.ReadNBytes(readerN, 3)

	// Assert
	assert.That(t, n, is.EqualTo(3), "count")
	assert.That(t, buffer, is.DeepEqualTo(expected), "buffer")
	assert.That[any](t, err, is.Nil, "error")
	assert.That(t, bufferN, is.DeepEqualTo(expected), "bufferN")
	assert.That[any](t, errN, is.Nil, "errorN")
}

func Test_ByteReader_MultiplePass(t *testing.T) {
	// Arrange
	expected := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	reader, readerN := makeTestReaders(expected, 5)

	// Act
	buffer := make([]byte, 8)
	n, err := core.ReadBytes(reader, buffer)
	bufferN, errN := core.ReadNBytes(readerN, 8)

	// Assert
	assert.That(t, n, is.EqualTo(8), "count")
	assert.That(t, buffer, is.DeepEqualTo([]byte{1, 2, 3, 4, 5, 6, 7, 8}), "buffer")
	assert.That[any](t, err, is.Nil, "error")
	assert.That(t, bufferN, is.DeepEqualTo([]byte{1, 2, 3, 4, 5, 6, 7, 8}), "bufferN")
	assert.That[any](t, errN, is.Nil, "errorN")
}

func Test_ByteReader_MultiplePass_LessThanRequired(t *testing.T) {
	// Arrange
	expected := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}
	reader, readerN := makeTestReaders(expected, 5)

	// Act
	buffer := make([]byte, 10)
	n, err := core.ReadBytes(reader, buffer)
	bufferN, errN := core.ReadNBytes(readerN, 10)

	// Assert
	assert.That(t, n, is.EqualTo(9), "count")
	assert.That(t, buffer[0:9], is.DeepEqualTo(expected), "buffer")
	assert.That(t, err, is.EqualTo(io.EOF), "error")
	assert.That(t, bufferN, is.DeepEqualTo(expected), "bufferN")
	assert.That(t, errN, is.EqualTo(io.EOF), "errorN")
}

// Reader capped at x bytes per read
type testReader struct {
	rdr io.Reader
	cap int
}

func makeTestReaders(buffer []byte, cap int) (testReader, testReader) {
	return testReader{bytes.NewReader(buffer), cap}, testReader{bytes.NewReader(buffer), cap}
}

func (r testReader) Read(buffer []byte) (n int, err error) {
	if r.cap >= len(buffer) {
		return r.rdr.Read(buffer)
	}
	buff2 := make([]byte, r.cap)
	n, err = r.rdr.Read(buff2)
	copy(buffer, buff2)
	return n, err
}
