package core

import (
	"fmt"
	"reflect"
	"strings"
)

// Clone an object, filling unset fields with defaults
func CloneWithDefaults[T any](original, defaults T, skip ...string) (result T) {
	return cloneWithDefaults(reflect.ValueOf(original), reflect.ValueOf(defaults), skip).Elem().Interface().(T)
}

func cloneWithDefaults(original, defaults reflect.Value, skip []string) (result reflect.Value) {
	objType := original.Type()
	result = reflect.New(objType)
	for fieldIdx := 0; fieldIdx < objType.NumField(); fieldIdx++ {
		fieldName := objType.Field(fieldIdx).Name
		if shouldNotSkip(fieldName, skip) && objType.Field(fieldIdx).IsExported() {
			value := original.Field(fieldIdx)
			if objType.Field(fieldIdx).Type.Kind() == reflect.Struct {
				// Iterate through subfields
				value = cloneWithDefaults(original.Field(fieldIdx), defaults.Field(fieldIdx), subSkipFields(fieldName, skip)).Elem()
			} else {
				// Get original value
				// Compare to default value
				defaultValue := getDefaultValue(value.Type())
				if reflect.DeepEqual(value.Interface(), defaultValue) {
					// Replace with default
					value = defaults.Field(fieldIdx)
				}
			}
			// Set new value
			result.Elem().Field(fieldIdx).Set(value)
		}
	}
	return result
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
var defaultValueCache = make(map[reflect.Type]any)

func shouldNotSkip(name string, skip []string) bool {
	for _, s := range skip {
		if name == s {
			return false
		}
	}
	return true
}

func subSkipFields(name string, skip []string) (newSkips []string) {
	name = fmt.Sprintf("%s.", name)
	for _, s := range skip {
		if strings.HasPrefix(s, name) {
			newSkips = append(newSkips, strings.TrimPrefix(s, name))
		}
	}
	return newSkips
}

func getDefaultValue(objType reflect.Type) (defaultVal any) {
	var ok bool
	if defaultVal, ok = defaultValueCache[objType]; !ok {
		defaultVal = reflect.New(objType).Elem().Interface()
		defaultValueCache[objType] = defaultVal
	}
	return defaultVal
}
