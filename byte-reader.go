package core

import (
	"io"
)

// Read an absolute number of bytes from a reader
func ReadNBytes(reader io.Reader, count int) (buffer []byte, err error) {
	buffer = make([]byte, count)
	var n int
	n, err = ReadBytes(reader, buffer)
	return buffer[:n], err
}

// Multiple pass reader
// Copes with io.Reader giving less bytes than requested
func ReadBytes(reader io.Reader, buffer []byte) (n int, err error) {
	soFar := 0
	for err == nil && soFar < len(buffer) {
		n, err = reader.Read(buffer[soFar:])
		soFar += n
	}
	return soFar, err
}
